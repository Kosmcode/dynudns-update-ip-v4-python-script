import logging
import requests
import os
import hashlib
from dotenv import load_dotenv

URL_ACTUAL_IP_ADDRESS = 'https://api.ipify.org?format=json'
FILEPATH_IP_LAST_RESPONSE = './ip_last_response'
FILEPATH_LOG = './change_ip.log'

logging.basicConfig(
    format='%(asctime)s [%(levelname)s] - %(message)s',
    filename=FILEPATH_LOG,
    level=logging.INFO,
)
logger = logging.getLogger()

load_dotenv()

dynudns_url = os.getenv('DYNUDNS_URL')
dynudns_username = os.getenv('DYNUDNS_USERNAME')
dynudns_password = os.getenv('DYNUDNS_PASSWORD')
dynudns_password_md5 = hashlib.md5(dynudns_password.encode()).hexdigest()


def is_change_ip_address(actual_ip):
    if not os.path.isfile(FILEPATH_IP_LAST_RESPONSE):
        actual_ip_last_response_handler = open(FILEPATH_IP_LAST_RESPONSE, 'w')
        actual_ip_last_response_handler.write(actual_ip)
        actual_ip_last_response_handler.close()

        return True

    actual_ip_last_response_handler = open(FILEPATH_IP_LAST_RESPONSE, 'r')
    last_ip_address = actual_ip_last_response_handler.read()

    if actual_ip == last_ip_address:
        return False

    actual_ip_last_response_handler = open(FILEPATH_IP_LAST_RESPONSE, 'w')
    actual_ip_last_response_handler.write(actual_ip)
    actual_ip_last_response_handler.close()

    return True


def get_actual_ip_address():
    actual_ip_request = requests.get(URL_ACTUAL_IP_ADDRESS)

    if actual_ip_request.status_code != 200:
        logger.error('Actual IP address response error')

        exit(1)

    if not actual_ip_request.json()['ip']:
        logger.error('Actual IP address response not has IP')

        exit(1)

    logger.info('Actual IP: ' + actual_ip_request.json()['ip'])

    return actual_ip_request.json()['ip']


actual_ip_request = get_actual_ip_address()

if not is_change_ip_address(actual_ip_request):
    logger.info('Not changed IP address')

    exit(0)

requests.get(
    dynudns_url, {
        'myip': actual_ip_request,
        'username': dynudns_username,
        'password': dynudns_password_md5,
    }
)

logger.info('Updated IP address')
