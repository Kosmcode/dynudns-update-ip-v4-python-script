# Python script to update ip address v4 on DynuDNS

## Usage

1. Install required packages
```bash
pip install -r requirements.txt
```

2. Copy example env and set variables (username and password)
```bash
cp .env.example .env
```

3. Run script
```bash
python3 dynudns-update-ip.py
```

ProTrick: Add this script to crontab on server